<?php
require('connection.php');
$response = array();
 if(isset($_POST['username'])){
   if(!empty($_POST['username'])){
        $username = $_POST['username'];
        $sql = $conn->prepare("DELETE FROM userinfo WHERE username=:username");
        $sql->bindParam(':username', $username);
        $sql->execute();
        if($sql->rowCount()){

            $stmt = $conn->prepare("SELECT * From user_files where receiver=:receiver OR sender=:sender");
            $stmt->execute(array(":receiver"=>$username,":sender"=>$username));
            $result=$stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach($result as $res){
               $id = $res['id'];
               $file = $res['file'];
               $filename = "userfiles/".$file;
               $sql = $conn->prepare("DELETE FROM user_files WHERE id=:id");
               $sql->bindParam(':id', $id);
               $sql->execute();
               unlink($filename);
           }
           $response["code"] = "1";
           
     }
     else{
         $response["code"] = "0";
      }
   }
}
   else{
     $response["code"] = "0";
    }
  echo json_encode($response);
?>