<?php
  require('connection.php');
  $directory = "./userfiles";
  $response = array();
  if(isset($_POST)){
       $sender = $_POST['sender'];
       $receiver = $_POST['receiver'];
       $File_name = $_FILES["userfile"]["name"];
       $Type  = $_FILES["userfile"]["type"] ;
       $Size   = ($_FILES["userfile"]["size"] / 1024); 
       $File_temp_name  = $_FILES["userfile"]["tmp_name"];
       if($Size<= 0){
          $response["code"] = -2;
	  die('cant not upload a file ');
       }
       if (file_exists($directory . " / " .  $_FILES["userfile"]["name"]))
       { 
           $response["code"] = -1;
           die($_FILES["userfile"]["name"] . " already exists. ");
       }

       if(is_uploaded_file($_FILES["userfile"]["tmp_name"])){ 
	  if(!move_uploaded_file($File_temp_name,$directory."/".$File_name)){
		    $response["code"] = -2;  
                    die('cant not file'.$File_name);
           }
          else{  
             $sql = $conn->prepare("INSERT INTO user_files set sender=:sender,receiver=:receiver,file=:file");              
             $sql->execute(array(":sender"=>$sender,":receiver"=>$receiver,":file"=>$File_name));
             if($sql) $response["code"] = 1;
             else     $response["code"] = -1;
          }
	}
	else
	{
            $response["code"] = 0;
	    die('attack on file');
        }	  
       echo json_encode($response);  
} 
?>