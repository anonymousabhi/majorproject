<?php

require('connection.php');
if($_POST['id']){ 
     $id = $_POST['id'];
     
     $stmt = $conn->prepare("SELECT * from user_files WHERE id=:id");
     $stmt->execute(array(":id"=>$id));
     $row=$stmt->fetch(PDO::FETCH_ASSOC);
     if($row){
         $file = $row["file"];
     }
     else{
         echo "file not found";
     } 
     $filename = "userfiles/".$file;
     header('Content-Disposition: attachment; filename="'.$filename.'"');
     readfile($filename);
     
        $sql = $conn->prepare("DELETE FROM user_files WHERE id=:id");
        $sql->bindParam(':id', $id);
        $sql->execute();
        unlink($filename);
}
?>