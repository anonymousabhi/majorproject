package algorithms;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import application.LoginModel;

public class User_Info {
	static ArrayList<User_Info> list = new ArrayList<User_Info>();
	public String iName;
	public String iUsername;
	public String iContact;
	public long iKey;
	public String iMac;
	static LoginModel model = new LoginModel();
	LoginModel loginModel = new LoginModel();

	public User_Info(String iName, String iUsername, String iContact, long iKey, String iMac) {
		this.iName = iName;
		this.iUsername = iUsername;
		this.iContact = iContact;
		this.iKey = iKey;
		this.iMac = iMac;

	}

	static User_Info temp_user = null;

	public static void prepareList() {
		list.clear();
		try {
			Document document = Jsoup.connect("http://consonant-vicinity.000webhostapp.com/retrievedata.php")
					.timeout(10000).post();
			StringTokenizer str = new StringTokenizer(document.text(), " ");

			while (str.hasMoreTokens()) {
				String name = str.nextToken();
				String username = str.nextToken();
				String contact = str.nextToken();
				Long key = Long.parseLong(str.nextToken());
				String mac = str.nextToken();
				if (!username.equals(model.getUsername())) {
					list.add(new User_Info(name, username, contact, key, mac));
				}

			}
		} catch (Exception e) {
			e.getMessage();
		}

	}

	public static void prepareList_Admin() {
		list.clear();
		try {
			Document document = Jsoup.connect("http://consonant-vicinity.000webhostapp.com/retrievedata.php")
					.timeout(10000).post();
			StringTokenizer str = new StringTokenizer(document.text(), " ");

			while (str.hasMoreTokens()) {
				String name = str.nextToken();
				String username = str.nextToken();
				String contact = str.nextToken();
				Long key = Long.parseLong(str.nextToken());
				String mac = str.nextToken();
				list.add(new User_Info(name, username, contact, key, mac));
			}
		} catch (Exception e) {
			e.getMessage();
		}

	}

	public static ArrayList<User_Info> admin_fetch_List() {
		return list;
	}

	public static ArrayList<User_Info> fetch_List() {
		return list;
	}

	public String get_Username() {
		return iUsername;
	}

	public String get_Contact() {
		return iContact;
	}

	public long get_Key() {
		return iKey;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return iName;
	}

	public String getMac() {
		// TODO Auto-generated method stub
		return iMac;
	}

}
