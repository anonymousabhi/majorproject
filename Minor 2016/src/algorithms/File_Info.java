package algorithms;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import application.LoginModel;

public class File_Info {
	static ArrayList<File_Info> list = new ArrayList<File_Info>();
	public String sender;
	public String sender_key;
	public String file_name;
	public String key;
	LoginModel model = new LoginModel();
	LoginModel loginModel = new LoginModel();

	public File_Info(String key, String sender, String sender_key, String file_name) {
		this.key = key;
		this.sender = sender;
		this.sender_key = sender_key;
		this.file_name = file_name;
	}

	public static void prepareList() {
		list.clear();
		try {
			LoginModel loginModel = new LoginModel();
			Document document = Jsoup.connect("http://consonant-vicinity.000webhostapp.com/receive_data.php")
					.data("receiver", loginModel.getUsername()).timeout(10000).post();
			// System.out.println(document.text());

			StringTokenizer str = new StringTokenizer(document.text(), " ");

			while (str.hasMoreTokens()) {
				String key = str.nextToken();
				String sender = str.nextToken();
				String sender_key = str.nextToken();
				String file_name = str.nextToken();
				list.add(new File_Info(key, sender, sender_key, file_name));
			}
		} catch (Exception e) {
			e.getMessage();
		}

	}

	public static ArrayList<File_Info> fetch_List() {
		return list;

	}

	public String get_sender() {
		return sender;
	}

	public String get_file_name() {
		return file_name;
	}

	public String get_key() {
		return key;
	}

	public String get_sender_key() {
		return sender_key;
	}

}
