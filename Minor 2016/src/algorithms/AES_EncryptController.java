package algorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import application.LoginModel;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AES_EncryptController implements Initializable {

	@FXML
	TextArea taTwo;

	String Rkey = "", Skey = "";
	@FXML
	Label lblAlgo_name;
	@FXML
	Label lblInfo;
	@FXML
	Label lblText_Type;
	@FXML
	ListView<User_Info> listView;
	ObservableList<User_Info> list;
	String sender_name, receiver_name;
	static String IV = "AAAAAAAAAAAAAAAA";
	byte[] ciphe;
	String cipher_Text = null;
	int a = 0;
	LoginModel model = new LoginModel();
	@FXML
	Button uploadbutton;
	@FXML
	Button save_send_button;
	@FXML
	Button encrypt_button;

	public void setAlgo_name(String algo_name) {
		// TODO Auto-generated method stub
		lblAlgo_name.setText(algo_name);
		// taTwo.setEditable(false);
	}

	void change() {
		taTwo.setWrapText(true);
		taTwo.setEditable(false);
		Skey = model.getKey();
		sender_name = model.getUsername();

		lblText_Type.setText("Plain Text:");
		list = FXCollections.observableArrayList(User_Info.fetch_List());
		listView.setItems(list);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				listView.requestFocus();
				listView.getSelectionModel().select(0);
				listView.getFocusModel().focus(0);
			}
		});
		listView.setCellFactory(new Callback<ListView<User_Info>, ListCell<User_Info>>() {

			@Override
			public ListCell<User_Info> call(ListView<User_Info> param) {
				ListCell<User_Info> cell = new ListCell<User_Info>() {
					@Override
					protected void updateItem(User_Info item, boolean bool) {
						super.updateItem(item, bool);
						if (item != null) {
							setText(item.get_Username());
						}
					}

				};
				return cell;
			}
		});
		taTwo.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				lblInfo.setText("");
			}
		});
		listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<User_Info>() {

			@Override
			public void changed(ObservableValue<? extends User_Info> observable, User_Info oldValue,
					User_Info newValue) {
				if (newValue != null) {
					// System.out.println(newValue.get_Username());
					taTwo.setEditable(true);

					if (taTwo.getText().length() == 0) {
						lblInfo.setTextFill(Color.RED);
						lblInfo.setText("*enter some plaintext");
					}
					receiver_name = newValue.get_Username().trim();
					Rkey = Long.toString(newValue.get_Key());
				}

			}

		});

		/*
		 * listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
		 * 
		 * @Override public void handle(MouseEvent arg0) {
		 * 
		 * User_Info user = listView.getSelectionModel().getSelectedItem(); if
		 * (user != null) { System.out.println(user.get_Username());
		 * taTwo.setEditable(true); lblInfo.setText("*enter some plaintext");
		 * flag = true; receiver_name = user.get_Username().trim(); //
		 * System.out.println(user.getName()+user.get_Key()); Rkey =
		 * Long.toString(user.get_Key()); // System.out.println(Rkey); //
		 * System.out.println(Skey); } }
		 * 
		 * });
		 */

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		model = new LoginModel();
		if (!model.getInternetConnection()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!");
			alert.setHeaderText(null);
			alert.setContentText("No internet connection!");
			alert.showAndWait();
			taTwo.setEditable(false);
			uploadbutton.setDisable(true);
			save_send_button.setDisable(true);
			encrypt_button.setDisable(true);
		} else {
			User_Info.prepareList();
			change();
		}
	}

	public void upload() {
		lblInfo.setText("");
		taTwo.setText("");
		BufferedReader b = null;
		FileChooser fc = new FileChooser();
		fc.setInitialDirectory(new File(System.getProperty("user.home")));
		fc.getExtensionFilters().addAll(new ExtensionFilter("TXT files", "*.txt"));

		File selectedFile = fc.showOpenDialog(null);

		if (selectedFile != null) {
			String s = selectedFile.getAbsolutePath();
			try {
				b = new BufferedReader(new FileReader(s));
				String str;
				while ((str = b.readLine()) != null) {
					taTwo.appendText(str);
					taTwo.appendText("\n");
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (b != null) {
					try {
						b.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public static byte[] encrypt(String plainText, String encryptionKey) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
		return cipher.doFinal(plainText.getBytes("UTF-8"));
	}

	private void encryptAlgo(String key) {
		a = 0;
		String plaintext = taTwo.getText().toString();
		String encryptionKey = key;
		while ((plaintext.length()) % 16 != 0) {
			plaintext = plaintext + " ";
		}
		ciphe = new byte[plaintext.length()];

		for (int j = 0; j < plaintext.length(); j = j + 16) {
			String s = "";
			s = s + plaintext.substring(j, j + 16);
			byte[] cip;
			try {
				cip = encrypt(s, encryptionKey);
				for (int k = 0; k < cip.length; k++) {
					ciphe[a++] = cip[k];
				}
				String encryptedText = "";
				for (int i = 0; i < ciphe.length; i++) {
					encryptedText = encryptedText + " " + String.valueOf(ciphe[i]);
				}
				taTwo.setWrapText(true);
				// taTwo.setText(encryptedText);
				cipher_Text = encryptedText;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	String generateKey(long p, long q, long a, long b) {
		long h = (long) Math.pow(q, a);
		h = h % p;
		long j = (long) Math.pow(h, b);
		j = j % p;
		return Long.toString(j);
	}

	public void encrypt() {

		lblInfo.setText("");
		if (taTwo.getText().toString().length() > 0) {
			String key = generateKey(8690333381690951l, 11091501631241l, Long.parseLong(Rkey), Long.parseLong(Skey));
			encryptAlgo(key);
			lblInfo.setTextFill(Color.GREEN);
			lblInfo.setText("*encyption done use save and send");
		} else {
			lblInfo.setTextFill(Color.RED);
			lblInfo.setText("*enter some plaintext");
		}
	}

	public void send_file(String sender, String receiver, File file) {

		Document document;
		try {
			FileWriter fileWriter = null;
			fileWriter = new FileWriter(file);
			fileWriter.write(cipher_Text);
			fileWriter.close();
			// System.out.println(sender + " " + receiver + " " +
			// file.getName());
			document = Jsoup.connect("http://consonant-vicinity.000webhostapp.com/ftp_filepost.php")
					.data("sender", sender).data("receiver", receiver)
					.data("userfile", file.getName(), new FileInputStream(file)).timeout(10000).post();
			// System.out.println(document.text());
			if (document.text().equals("{\"code\":1}")) {
				// lblInfo.setTextFill(Color.GREEN);
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Success!");
				alert.setHeaderText(null);
				alert.setContentText("File save and send is successful!");
				alert.showAndWait();
			} // lblInfo.setText("*file save and send successfully");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			// lblInfo.setTextFill(Color.RED);
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Failure!");
			alert.setHeaderText(null);
			alert.setContentText("File save and send is unsuccessful!");
			alert.showAndWait();
			// lblInfo.setText("*file send unsuccessful\nplease check your
			// internet connection");

		}

	}

	public void send(ActionEvent event) {
		lblInfo.setText("");
		if (cipher_Text != null) {
			FileChooser fc = new FileChooser();
			fc.setInitialDirectory(new File(System.getProperty("user.home")));
			fc.getExtensionFilters().addAll(new ExtensionFilter("TXT (*.txt)", "*.txt"));
			File selected_File = fc.showSaveDialog(null);
			// System.out.println(selected_File.getName());
			if (selected_File != null) {
				send_file(sender_name, receiver_name, selected_File);
			}
		} else {
			lblInfo.setTextFill(Color.RED);
			lblInfo.setText("*please encrypt and then send");
		}
	}

	public void home(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/application/Dashboard.fxml").openStream());
			Scene scene = new Scene(root);
			// scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Dashboard");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

}
