package admin;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;

import java.util.ResourceBundle;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import algorithms.User_Info;
import application.LoginModel;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

public class AdminController implements Initializable {
	@FXML
	ListView<User_Info> listView;
	ObservableList<User_Info> list;
	@FXML
	Label lblusername;
	@FXML
	Label lblname;
	@FXML
	Label lblcontact;
	@FXML
	Label lblkey;
	@FXML
	Label lblmac;
	@FXML
	Label lbltxt;
	LoginModel model;
	@FXML
	Button delete_button;

	// Event Listener on Button.onAction
	@FXML
	public void delete_user(ActionEvent event) {

		User_Info user = listView.getSelectionModel().getSelectedItem();

		try {

			Document document = Jsoup.connect("http://consonant-vicinity.000webhostapp.com/deleteuser.php")
					.data("username", user.iUsername).timeout(10000).post();
			// System.out.println(document.text());
			if (document.text().equals("{\"code\":\"1\"}")) {
				list.remove(user);
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Success!");
				alert.setHeaderText(null);
				alert.setContentText("Delete Successful!");
				alert.showAndWait();
				lblusername.setText("Username:");
				lblname.setText("Name:");
				lblkey.setText("Key:");
				lblmac.setText("Mac:");
				lblcontact.setText("Contact:");
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error!");
				alert.setHeaderText(null);
				alert.setContentText("Delete Unsuccessful!");
				alert.showAndWait();
			}
		} catch (Exception e) {
			e.getMessage();
		}
		listView.getSelectionModel().clearSelection();
		list.addListener(new ListChangeListener<User_Info>() {

			@Override
			public void onChanged(Change c) {
				// TODO Auto-generated method stub
				User_Info.prepareList();
				change();
			}

		});
	}

	public void change() {
		list = FXCollections.observableArrayList(User_Info.admin_fetch_List());
		listView.setItems(list);
		if (list.size() != 0) {
			lbltxt.setTextFill(Color.RED);
			lbltxt.setText("");
		}
		// listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		listView.setCellFactory(new Callback<ListView<User_Info>, ListCell<User_Info>>() {

			@Override
			public ListCell<User_Info> call(ListView<User_Info> param) {
				ListCell<User_Info> cell = new ListCell<User_Info>() {
					@Override
					protected void updateItem(User_Info item, boolean bool) {
						super.updateItem(item, bool);
						if (item != null) {
							setText(item.get_Username());
						} else {
							setText("");
						}
					}

				};
				return cell;
			}
		});
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				listView.requestFocus();
				listView.getSelectionModel().select(0);
				listView.getFocusModel().focus(0);
			}
		});
		listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<User_Info>() {

			@Override
			public void changed(ObservableValue<? extends User_Info> observable, User_Info oldValue,
					User_Info newValue) {
				if (newValue != null) {
					lblusername.setText("Username: " + newValue.get_Username());
					lblname.setText("Name: " + newValue.getName());
					lblcontact.setText("Contact: " + newValue.get_Contact());
					lblkey.setText("Key: " + newValue.get_Key() + " ");
					lblmac.setText("Mac: " + newValue.getMac());
				}

			}

		});

		/*
		 * listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
		 * 
		 * @Override public void handle(MouseEvent arg0) { lbltxt.setText("");
		 * User_Info user = listView.getSelectionModel().getSelectedItem(); if
		 * (user != null) { lblusername.setText("Username: " +
		 * user.get_Username()); lblname.setText("Name: " + user.getName());
		 * lblcontact.setText("Contact: " + user.get_Contact());
		 * lblkey.setText("Key: " + user.get_Key() + " ");
		 * lblmac.setText("Mac: " + user.getMac() + " "); } }
		 * 
		 * });
		 */

	}

	@FXML
	private void logout(ActionEvent event) {
		// TODO Auto-generated method stub
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/application/AdminLogin.fxml").openStream());
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Admin");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

		model = new LoginModel();
		if (!model.getInternetConnection()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!");
			alert.setHeaderText(null);
			alert.setContentText("No internet connection!");
			alert.showAndWait();
			delete_button.setDisable(true);
		} else {
			User_Info.prepareList_Admin();
			change();
		}

	}

}
