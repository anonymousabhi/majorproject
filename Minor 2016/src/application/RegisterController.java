package application;

import java.io.IOException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import java.util.ResourceBundle;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class RegisterController implements Initializable {
	LoginModel model;
	@FXML
	private CheckBox cbRemember;

	public void login(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Login.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Log in");
			primaryStage.show();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}
	}

	public void call_login(ActionEvent event, String username) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Login.fxml").openStream());
			Scene scene = new Scene(root);
			LoginController controller = (LoginController) loader.getController();
			controller.set(username);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Log in");
			primaryStage.show();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}
	}

	public void home(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Main.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Home");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public LoginModel loginModel = new LoginModel();

	@FXML
	private Label isConnected;

	@FXML
	private TextField txtUsername;
	@FXML
	private TextField txtName;
	@FXML
	private TextField txtCNo;
	@FXML
	private TextField txtPassword;
	ObservableList<String> list = FXCollections.observableArrayList("", "", "");
	@FXML
	AnchorPane anchorPane;
	@FXML
	Button register_Button;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		/*
		 * if (loginModel.isDbConnected()) { isConnected.setText("Connected!");
		 * } else { isConnected.setText("*not connected!"); }
		 * 
		 */
		model = new LoginModel();
		if (!model.getInternetConnection()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!");
			alert.setHeaderText(null);
			alert.setContentText("No internet connection!");
			alert.showAndWait();
			register_Button.setDisable(true);
		}

	}

	String getMac() {
		try {
			// InetAddress ip = InetAddress.getLocalHost();
			// System.out.println(ip.getHostAddress());
			Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
			while (networks.hasMoreElements()) {
				NetworkInterface network = networks.nextElement();
				byte[] mac = network.getHardwareAddress();

				if (mac != null) {
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
					}
					// System.out.println(sb.toString());
					return sb.toString();
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return null;

	}

	boolean Jsoup_Register(String name, String c_no, String username, String password) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error!");
		alert.setHeaderText(null);
		try {
			Document document = Jsoup.connect("http://consonant-vicinity.000webhostapp.com/uregistration.php")
					.data("username", username).data("name", name).data("phone", c_no).data("password", password)
					.data("mac_address", getMac()).timeout(10000).post();
			// System.out.println(document.text());
			if (document.text().equals("{\"code\":\"1\"}")) {
				return true;
			} else {
				if (document.text().equals("{\"code\":\"-1\"}")) {
					alert.setContentText("Computer already registered contact admin!");
				} else {
					alert.setContentText("Username already exists!");
				}
				alert.showAndWait();
				return false;
			}
		} catch (Exception e) {
			// e.getMessage();
			return false;
		}
	}

	public void register(ActionEvent event) {
		String name = txtName.getText();
		String username = txtUsername.getText();
		String password = txtPassword.getText();
		String contact_no = txtCNo.getText();
		try {
			if (name.length() != 0 && username.length() != 0 && password.length() != 0) {
				if (contact_no.length() == 10) {
					if (password.length() >= 8) {
						boolean b = Jsoup_Register(name, contact_no, username, password); // loginModel.register(name,
																							// contact_no,
																							// username,
																							// password);
						if (b) {
							txtName.setText("");
							txtCNo.setText("");
							txtUsername.setText("");
							txtPassword.setText("");
							call_login(event, username);
						}
					} else {
						isConnected.setText("*password is too short!");
					}
				} else {
					isConnected.setText("*10 digits contact number required");
				}
			} else {
				isConnected.setText("*one or more filed is empty!");
			}
		} catch (Exception e) {
			isConnected.setText("*exception occurred");
		}
	}
}
