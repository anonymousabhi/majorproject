package application;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	LoginModel model;
	

	@Override
	public void start(Stage primaryStage) {
		model = new LoginModel();
		try {
			Parent root;
			if (model.getLoggin()) {
				root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
			} else {
				root = FXMLLoader.load(getClass().getResource("Main.fxml"));
			}
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Home");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
