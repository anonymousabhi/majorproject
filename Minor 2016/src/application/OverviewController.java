package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class OverviewController implements Initializable {

	@FXML
	Label txtflw;
	String s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtflw.setWrapText(true);
		txtflw.setText("OVERVIEW:\n\n"
				+ "Since ages, Security has always been an integral part for existence of anything. Encryption has come up as a solution, and plays an important role in information security system. Any encryption software uses an encryption scheme that encodes computer data so that it cannot be recovered without the correct key. Software encryption is a fundamental part of modern computer communications and file protection. We need a dynamic application with a perfect GUI which enables us to encrypt and decrypt a file using any algorithm. Thus a java based application kryptόs has been built to serve our purpose. In the backend PHP has been used for management."
				);
	}

	public void back(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = null;
			if (s == "Dashboard") {
				root = loader.load(getClass().getResource("Dashboard.fxml").openStream());
				primaryStage.setTitle("Dashboard");
			} else {
				root = loader.load(getClass().getResource("Main.fxml").openStream());
				primaryStage.setTitle("Home");
			}
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);

			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public void from(String string) {
		// TODO Auto-generated method stub
		s = string;
	}

}
