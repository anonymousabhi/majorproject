package application;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.io.IOException;
import javafx.event.ActionEvent;

public class TutorialsController {
	String s;

	// Event Listener on Hyperlink.onAction
	@FXML
	public void AES(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Tutorial_description.fxml").openStream());
			Scene scene = new Scene(root);
			Tutorial_descriptionController fxmlLoader = (Tutorial_descriptionController) loader.getController();
			fxmlLoader.setContent("AES");
			primaryStage.setScene(scene);
			primaryStage.setTitle("AES");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	// Event Listener on Hyperlink.onAction
	@FXML
	public void RSA(ActionEvent event) {

		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Tutorial_description.fxml").openStream());
			Scene scene = new Scene(root);
			Tutorial_descriptionController fxmlLoader = (Tutorial_descriptionController) loader.getController();
			fxmlLoader.setContent("Diffie");
			primaryStage.setScene(scene);
			primaryStage.setTitle("Diffie Hellman");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public void back(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = null;
			if (s == "Dashboard") {
				root = loader.load(getClass().getResource("Dashboard.fxml").openStream());
				primaryStage.setTitle("Dashboard");
			} else {
				root = loader.load(getClass().getResource("Main.fxml").openStream());
				primaryStage.setTitle("Home");
			}
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public void from(String string) {
		// TODO Auto-generated method stub
		s = string;
	}

}
