package application;

import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.prefs.Preferences;

public class LoginModel {
	Connection connection;
	private Preferences prefs;

	public LoginModel() {
		prefs = Preferences.userRoot().node(this.getClass().getName());

		/*
		 * connection = SqliteConnection.connector(); if (connection == null) {
		 * System.out.println("Connection is unsuccesful"); System.exit(1); }
		 */
	}

	public boolean getInternetConnection() {
		try {
			URL url = new URL("http://www.instanceofjava.com");

			URLConnection connection = url.openConnection();
			connection.connect();
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	public void setLoggedin(boolean islogin, String username, String password, String key) {
		prefs.putBoolean("login", islogin);
		prefs.put("username", username);
		prefs.put("password", password);
		prefs.put("key", key);
	}

	public String getKey() {
		return prefs.get("key", null);
	}

	public boolean getLoggin() {
		return prefs.getBoolean("login", false);
	}

	public String getUsername() {
		return prefs.get("username", null);
	}

	public boolean isDbConnected() {
		try {
			return !connection.isClosed();
		} catch (SQLException e) {
			return false;
		}
	}

	/*
	 * public boolean isLogin(String user, String pass, boolean isLogin) throws
	 * SQLException { PreparedStatement preparedStatement = null; ResultSet
	 * resultSet = null; String query =
	 * "SELECT * FROM user_info where username = ? AND password = ?"; try {
	 * preparedStatement = connection.prepareStatement(query);
	 * preparedStatement.setString(1, user); preparedStatement.setString(2,
	 * pass); resultSet = preparedStatement.executeQuery(); if
	 * (resultSet.next()) { setLoggedin(isLogin, user, pass); return true; }
	 * else { return false; } } catch (SQLException e) {
	 * System.out.println("Exception in isLogin");
	 * System.out.println(e.getMessage()); return false; } finally { if
	 * (preparedStatement != null) { preparedStatement.close(); } if (resultSet
	 * != null) { resultSet.close(); } } }
	 * 
	 * public boolean register(String name, String c_no, String username, String
	 * password) throws SQLException { PreparedStatement preparedStatement =
	 * null; String query =
	 * "INSERT INTO user_info (name, age,username,password) VALUES (?, ?,?,?)";
	 * try { preparedStatement = connection.prepareStatement(query);
	 * preparedStatement.setString(1, name); preparedStatement.setString(2,
	 * c_no); preparedStatement.setString(3, username);
	 * preparedStatement.setString(4, password); int b =
	 * preparedStatement.executeUpdate(); if (b == 1) { return true; } else {
	 * return false; }
	 * 
	 * } catch (SQLException e) { System.out.println("Exception in isLogin");
	 * System.out.println(e.getMessage()); return false; } finally {
	 * preparedStatement.close(); } }
	 */
}
