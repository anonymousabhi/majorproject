package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class FeedbackController implements Initializable {
	@FXML
	private WebView view;
	private WebEngine engine;
	String s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		engine = view.getEngine();
		// engine.loadContent("<html><body><h1>Hello
		// world!</h1></body></html>");
		engine.load("http://bit.ly//projfeedback");
	}

	public void back(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = null;
			if (s == "Dashboard") {
				root = loader.load(getClass().getResource("Dashboard.fxml").openStream());
				primaryStage.setTitle("Dashboard");
			} else {
				root = loader.load(getClass().getResource("Main.fxml").openStream());
				primaryStage.setTitle("Home");
			}
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public void from(String string) {
		// TODO Auto-generated method stub
		s = string;
	}

}
