package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class About_usController implements Initializable {
	@FXML
	Label txtflw;
	String s;

	public void back(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = null;
			if (s == "Dashboard") {
				root = loader.load(getClass().getResource("Dashboard.fxml").openStream());
				primaryStage.setTitle("Dashboard");
			} else {
				root = loader.load(getClass().getResource("Main.fxml").openStream());
				primaryStage.setTitle("Home");
			}
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);

			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public void from(String string) {
		// TODO Auto-generated method stub
		s = string;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtflw.setWrapText(true);
		txtflw.setText("ABOUT US:\n\n"
				+ "This is to certify that the project entitled ‘Transfer of file using cryptography’ submitted to Department of Computer Science, University of Delhi of the requirements for the award of the Degree of BTECH (Computer Science) by Abhishek Thakur, Kaustubh Agarwal and Yogita Yadav under the supervision and guidance of Dr. Sameer Anand, Assistant Professor, Department of Computer Science, Shaheed Sukhdev College of Business Studies (University of Delhi) Vivek Vihar, Delhi.");
	}

}
