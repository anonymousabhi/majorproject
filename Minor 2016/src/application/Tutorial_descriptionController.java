package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Tutorial_descriptionController implements Initializable {
	@FXML
	Label txtflw;

	public void back(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Tutorials.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Tutorials");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		txtflw.setWrapText(true);
	}

	public void setContent(String string) {
		if (string.equals("AES")) {
			txtflw.setText("ADVANCED ENCRYPTION STANDARD:\n\n"
					+ "The more popular and widely adopted symmetric encryption algorithm likely to be encountered nowadays is the Advanced Encryption Standard (AES). It is found at least six time faster than triple DES."
					+ "A replacement for DES was needed as its key size was too small. With increasing computing power, it was considered vulnerable against exhaustive key search attack. Triple DES was designed to overcome this drawback but it was found slow."
					+ "The features of AES are as follows −\n" + "•	Symmetric key symmetric block cipher\n"
					+ "•	128-bit data, 128/192/256-bit keys\n" + "•	Stronger and faster than Triple-DES\n"
					+ "•	Provide full specification and design details\n"
					+ "•	Software implementable in C and Java\n"
					+ "AES is an iterative rather than Feistel cipher. It is based on ‘substitution–permutation network’. It comprises of a series of linked operations, some of which involve replacing inputs by specific outputs (substitutions) and others involve shuffling bits around (permutations)."
					+ "Interestingly, AES performs all its computations on bytes rather than bits. Hence, AES treats the 128 bits of a plaintext block as 16 bytes. These 16 bytes are arranged in four columns and four rows for processing as a matrix −"
					+ "Unlike DES, the number of rounds in AES is variable and depends on the length of the key. AES uses 10 rounds for 128-bit keys, 12 rounds for 192-bit keys and 14 rounds for 256-bit keys. Each of these rounds uses a different 128-bit round key, which is calculated from the original AES key.");
		} else if (string.equals("Diffie")) {
			txtflw.setText("Diffie Hellman:                                          \n\n"
					+ "The Diffie-Hellman algorithm was developed by Whitfield Diffie and Martin Hellman in 1976. This algorithm was devised not to encrypt the data but to generate same private cryptographic key at both ends so that there is no need to transfer this key from one communication end to another. Though this algorithm is a bit slow but it is the sheer power of this algorithm that makes it so popular in encryption key generation."
					+ "This algorithm uses arithmetic modulus as the basis of its calculation. Suppose Alice and Bob follow this key exchange procedure with Eve acting as a man in middle interceptor (or the bad person).");
		}

	}
}
