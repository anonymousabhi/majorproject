package application;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import algorithms.AES_EncryptController;
import algorithms.File_Info;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class DashboardController implements Initializable {

	LoginModel model = new LoginModel();
	@FXML
	private Label lblUsername;

	public void getUser(String user) {
		// TODO Auto-generated method stub
		lblUsername.setText("Welcome " + user + " ");
	}

	public void overview(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Overview.fxml").openStream());
			Scene scene = new Scene(root);
			OverviewController controller = (OverviewController) loader.getController();
			controller.from("Dashboard");
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Overview");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public void tutorials(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Tutorials.fxml").openStream());
			Scene scene = new Scene(root);
			TutorialsController controller = (TutorialsController) loader.getController();
			controller.from("Dashboard");
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Tutorials");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}
	}

	public void feedback(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Feedback.fxml").openStream());
			Scene scene = new Scene(root);
			FeedbackController controller = (FeedbackController) loader.getController();
			controller.from("Dashboard");
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Feedback");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}
	}

	public void about_us(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("About_us.fxml").openStream());
			Scene scene = new Scene(root);
			About_usController controller = (About_usController) loader.getController();
			controller.from("Dashboard");
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("About Us");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	// Event Listener on Hyperlink.onAction
	@FXML
	public void logout(ActionEvent event) {
		try {
			model.setLoggedin(false, "", "", "");
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Main.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Home");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	// Event Listener on Button.onAction
	@FXML
	public void send_Files(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = null;
			root = loader.load(getClass().getResource("/algorithms/AES_Encrypt.fxml").openStream());
			AES_EncryptController fxmlLoader = (AES_EncryptController) loader.getController();
			fxmlLoader.setAlgo_name("Send Files");
			primaryStage.setTitle("Send Files");
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException exception) {

		}

	}

	// Event Listener on Button.onAction
	@FXML
	public void recieve_Files(ActionEvent event) {
		try {
			File_Info.prepareList();
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = null;
			root = loader.load(getClass().getResource("/algorithms/Receive_Files.fxml").openStream());
			primaryStage.setTitle("Receive Files");
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException exception) {

		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		model = new LoginModel();
		if (model.getInternetConnection()) {
			
		}
		getUser(model.getUsername());

	}
}
