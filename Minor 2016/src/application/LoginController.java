package application;

import java.io.IOException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import java.util.ResourceBundle;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.Node;;

public class LoginController implements Initializable {
	public LoginModel loginModel = new LoginModel();

	@FXML
	private Label isConnected;

	@FXML
	private TextField txtUsername;

	@FXML
	private TextField txtPassword;
	@FXML
	private CheckBox cbRemember;
	LoginModel model;
	@FXML
	Button login_Button;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		/*
		 * if (loginModel.isDbConnected()) { isConnected.setText("Connected!");
		 * } else { isConnected.setText("*not connected!"); }
		 */
		// isConnected.setTextFill(Color.RED);
		model = new LoginModel();
		if (!model.getInternetConnection()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!");
			alert.setHeaderText(null);
			alert.setContentText("No internet connection!");
			alert.showAndWait();
			login_Button.setDisable(true);
		}

	}

	static String getMac() {
		try {
			// InetAddress ip = InetAddress.getLocalHost();
			// System.out.println(ip.getHostAddress());
			Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
			while (networks.hasMoreElements()) {
				NetworkInterface network = networks.nextElement();
				byte[] mac = network.getHardwareAddress();

				if (mac != null) {
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
					}
					// System.out.println(sb.toString());
					return sb.toString();
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return null;

	}

	public void admin(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("AdminLogin.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Admin");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public void home(ActionEvent event) {
		try {
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("Main.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Home");
			primaryStage.show();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.getMessage();
		}

	}

	public boolean Jsoup_login(String username, String password, boolean isChecked) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error!");
		alert.setHeaderText(null);

		try {
			Document document = Jsoup.connect("http://consonant-vicinity.000webhostapp.com/ulogin.php")
					.data("username", username).data("password", password).data("mac_address", getMac()).timeout(10000)
					.post();
			if (document.text().equals("{\"code\":\"0\"}")) {
				alert.setContentText("Wrong username or password!");
				alert.showAndWait();
				return false;
			} else if (document.text().equals("{\"code\":\"-1\"}")) {
				alert.setContentText("This is not your registered pc\ncontact admin!");
				alert.showAndWait();
				return false;
			} else {
				String key = document.text();
				// System.out.println(key.substring(9,key.length()-2));
				loginModel.setLoggedin(isChecked, username, password, key.substring(9, key.length() - 2));
				return true;
			}
			// System.out.println(document.text());
		} catch (Exception e) {
			e.getMessage();
			return false;
		}
	}

	public void login(ActionEvent event) {
		isConnected.setText("");
		isConnected.setTextFill(Color.RED);
		try {
			String username = txtUsername.getText(), password = txtPassword.getText();
			boolean b = Jsoup_login(username, password, cbRemember.isSelected());
			// if (loginModel.isLogin(txtUsername.getText(),
			// txtPassword.getText(), cbRemember.isSelected())) {
			if (b) {
				((Node) event.getSource()).getScene().getWindow().hide();
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("Dashboard.fxml").openStream());
				Scene scene = new Scene(root);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.setTitle("Dashboard");
				primaryStage.show();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void set(String username) {
		// TODO Auto-generated method stub
		txtUsername.setText(username);
		isConnected.setTextFill(Color.GREEN);
		isConnected.setText("Registration successful \n Please Login!");
		// isConnected.setTextFill(Color.RED);
	}
}
